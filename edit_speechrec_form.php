<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the editing form for the speechrec question type.
 *
 * @package    qtype
 * @subpackage speechrec
 * @copyright  2007 Jamie Pratt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Short answer question editing form definition.
 *
 * @copyright  2007 Jamie Pratt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_speechrec_edit_form extends question_edit_form {

    protected function definition_inner($mform) {
        $menu = array(
            'af-ZA' => 'Afrikaans',
            'id-ID' => 'Bahasa Indonesia',
            'ms-MY' => 'Bahasa Melayu',
            'ca-ES' => 'Català',
            'cs-CZ' => 'Čeština',
            'de-DE' => 'Deutsch',
            'en-AU' => 'English (Australia)',
            'en-CA' => 'English (Canada)',
            'en-IN' => 'English (India)',
            'en-NZ' => 'English (New Zealand)',
            'en-ZA' => 'English (South Africa)',
            'en-UK' => 'English (United Kingdom)',
            'en-US' => 'English (United States)',
            'es-AR' => 'Español (Argentina)',
            'es-BO' => 'Español (Bolivia)',
            'es-CL' => 'Español (Chile)',
            'es-CO' => 'Español (Colombia)',
            'es-CR' => 'Español (Costa Rica)',
            'es-EC' => 'Español (Ecuador)',
            'es-SV' => 'Español (El Salvador)',
            'es-ES' => 'Español (España)',
            'es-US' => 'Español (Estados Unidos)',
            'es-GT' => 'Español (Guatemala)',
            'es-HN' => 'Español (Honduras)',
            'es-MX' => 'Español (México)',
            'es-NI' => 'Español (Nicaragua)',
            'es-PA' => 'Español (Panamá)',
            'es-PY' => 'Español (Paraguay)',
            'es-PE' => 'Español (Perú)',
            'es-PR' => 'Español (Puerto Rico)',
            'es-DO' => 'Español (República Dominicana)',
            'es-UY' => 'Español (Uruguay)',
            'es-VE' => 'Español (Venezuela)',
            'eu-ES' => 'Euskara',
            'fr-FR' => 'Français',
            'fr-CA' => 'Français (Canada)',
            'gl-ES' => 'Galego',
            'hr_HR' => 'Hrvatski',
            'zu-ZA' => 'IsiZulu',
            'is-IS' => 'Íslenska',
            'it-IT' => 'Italiano (Italia)',
            'it-CH' => 'Italiano (Svizzera)',
            'hu-HU' => 'Magyar',
            'nl-NL' => 'Nederlands',
            'nb-NO' => 'Norsk bokmål',
            'pl-PL' => 'Polski',
            'pt-BR' => 'Português (Brasil)',
            'pt-PT' => 'Português (Portugal)',
            'ro-RO' => 'Română',
            'sk-SK' => 'Slovenčina',
            'fi-FI' => 'Suomi',
            'sv-SE' => 'Svenska',
            'tr-TR' => 'Türkçe',
            'bg-BG' => 'български',
            'ru-RU' => 'Pусский',
            'sr-RS' => 'Српски',
            'ko-KR' => '한국어',
            'cmn-Hans-CN' => '中文 (普通话 (中国大陆))',
            'cmn-Hans-HK' => '中文 (普通话 (香港))',
            'cmn-Hant-TW' => '中文 (中文 (台灣))',
            'yue-Hant-HK' => '中文 (粵語 (香港))',
            'ja-JP' => '日本語',
            'la' => 'Lingua latīna',
        );

        $truefalse = array(
            'true' => get_string('true', 'qtype_speechrec'),
            'false' => get_string('false', 'qtype_speechrec')
        );

        $mform->addElement('select', 'sr_lang', get_string('language', 'qtype_speechrec'), $menu);
        $mform->addElement('select', 'showhelp', get_string('showhelp', 'qtype_speechrec'), $truefalse);
        $mform->addHelpButton("showhelp", "showhelp", "qtype_speechrec");
        $mform->addElement('select', 'showinterim', get_string('showinterim', 'qtype_speechrec'), $truefalse);
        $mform->addHelpButton("showinterim", "showinterim", "qtype_speechrec");
        $mform->addElement('static', 'answersinstruct', get_string('correctanswers', 'qtype_speechrec'), get_string('filloutoneanswer', 'qtype_speechrec'));
        $mform->closeHeaderBefore('answersinstruct');

        $this->add_per_answer_fields($mform, get_string('answerno', 'qtype_speechrec', '{no}'), question_bank::fraction_options(), 1, 0);

        $this->add_interactive_settings();
    }

    protected function get_more_choices_string() {
        return get_string('addmoreanswerblanks', 'qtype_speechrec');
    }

    protected function data_preprocessing($question) {
        $question = parent::data_preprocessing($question);
        $question = $this->data_preprocessing_answers($question);
        $question = $this->data_preprocessing_hints($question);

        return $question;
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        $answers = $data['answer'];
        $answercount = 0;
        $maxgrade = false;
        foreach ($answers as $key => $answer) {
            $trimmedanswer = trim($answer);
            if ($trimmedanswer !== '') {
                $answercount++;
                if ($data['fraction'][$key] == 1) {
                    $maxgrade = true;
                }
            } else if ($data['fraction'][$key] != 0 ||
                    !html_is_blank($data['feedback'][$key]['text'])) {
                $errors["answeroptions[{$key}]"] = get_string('answermustbegiven', 'qtype_speechrec');
                $answercount++;
            }
        }
        if ($answercount == 0) {
            $errors['answeroptions[0]'] = get_string('notenoughanswers', 'qtype_speechrec', 1);
        }
        if ($maxgrade == false) {
            $errors['answeroptions[0]'] = get_string('fractionsnomax', 'question');
        }
        return $errors;
    }

    public function qtype() {
        return 'speechrec';
    }

}
