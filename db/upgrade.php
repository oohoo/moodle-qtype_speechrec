<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Short-answer question type upgrade code.
 *
 * @package    qtype
 * @subpackage speechrec
 * @copyright  2011 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Upgrade code for the essay question type.
 * @param int $oldversion the version we are upgrading from.
 */
function xmldb_qtype_speechrec_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2015071901) {

        // Define field usecase to be dropped from qtype_speechrec_options.
        $table = new xmldb_table('qtype_speechrec_options');
        $field = new xmldb_field('usecase');

        // Conditionally launch drop field usecase.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define field sr_lang to be added to qtype_speechrec_options.
        $table = new xmldb_table('qtype_speechrec_options');
        $field = new xmldb_field('sr_lang', XMLDB_TYPE_CHAR, '10', null, null, null, 'en_US', 'questionid');

        // Conditionally launch add field sr_lang.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Speechrec savepoint reached.
        upgrade_plugin_savepoint(true, 2015071901, 'qtype', 'speechrec');
    }

    if ($oldversion < 2015072000) {

        // Define field showhelp to be added to qtype_speechrec_options.
        $table = new xmldb_table('qtype_speechrec_options');
        $field = new xmldb_field('showhelp', XMLDB_TYPE_CHAR, '6', null, null, null, 'true', 'sr_lang');

        // Conditionally launch add field showhelp.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define field showinterim to be added to qtype_speechrec_options.
        $table = new xmldb_table('qtype_speechrec_options');
        $field = new xmldb_field('showinterim', XMLDB_TYPE_CHAR, '6', null, null, null, 'true', 'showhelp');

        // Conditionally launch add field showinterim.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Speechrec savepoint reached.
        upgrade_plugin_savepoint(true, 2015072000, 'qtype', 'speechrec');
    }

    return true;
}
