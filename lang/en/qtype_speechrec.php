<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_shortanswer', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    qtype
 * @subpackage shortanswer
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addmoreanswerblanks'] = 'Blanks for {no} More Answers';
$string['answer'] = 'Answer: {$a}';
$string['answermustbegiven'] = 'You must enter an answer if there is a grade or feedback.';
$string['answerno'] = 'Answer {$a}';
$string['browser'] = 'You are currently using a browser that is not compatible with this question type.<br>'
        . 'Please use Google Chrome. You can download it for free from <a href="https://support.google.com/chrome/answer/95346?hl=en" target="_blank">here</a>';
$string['caseno'] = 'No, case is unimportant';
$string['casesensitive'] = 'Case sensitivity';
$string['caseyes'] = 'Yes, case must match';
$string['correctansweris'] = 'The correct answer is: {$a}';
$string['correctanswers'] = 'Correct answers';
$string['false'] = 'No';
$string['filloutoneanswer'] = 'This currently only works with Chrome browser';
$string['notenoughanswers'] = 'This type of question requires at least {$a} answers';
$string['pleaseenterananswer'] = 'Please enter an answer.';
$string['pluginname'] = 'Speech recognition';
$string['pluginname_help'] = 'Speech recognition';
$string['pluginname_link'] = 'question/type/speechrec';
$string['pluginnameadding'] = 'Adding a speech recognition question';
$string['pluginnameediting'] = 'Editing a speech recognition question';
$string['pluginnamesummary'] = 'Allows speech recognition.';
$string['showinterim'] = 'Show Interim';
$string['showinterim_help'] = 'Show users the text they are saying as they record.';
$string['showhelp'] = 'Show help';
$string['showhelp_help'] = 'Allow users to view instructions on how to use the microphone.';
$string['true'] = 'Yes';

//Languages
$string['language'] = 'Language';
$string['french_ca'] = 'French (Canada)';
$string['french_fr'] = 'French (France)';
$string['english_us'] = 'English (United States)';
$string['english_gb'] = 'English (Great Britan)';
