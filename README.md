# Oohoo Speech Recognition Question Type #

This question type uses Google Chrome's speech API in order to compare spoken speech to the equivalent string value. This, helps in detecting proper pronunciation of words.

## Supported languages ##
	Afrikaans
	Bahasa Indonesia
	Bahasa Melayu
	Català
	Čeština
	Deutsch
	English (Australia)
	English (Canada)
	English (India)
	English (New Zealand)
	English (South Africa)
	English (United Kingdom)
	English (United States)
	Español (Argentina)
	Español (Bolivia)
	Español (Chile)
	Español (Colombia)
	Español (Costa Rica)
	Español (Ecuador)
	Español (El Salvador)
	Español (España)
	Español (Estados Unidos)
	Español (Guatemala)
	Español (Honduras)
	Español (México)
	Español (Nicaragua)
	Español (Panamá)
	Español (Paraguay)
	Español (Perú)
	Español (Puerto Rico)
	Español (República Dominicana)
	Español (Uruguay)
	Español (Venezuela)
	Euskara
	Français
	Galego
	Hrvatski
	IsiZulu
	Íslenska
	Italiano (Italia)
	Italiano (Svizzera)
	Magyar
	Nederlands
	Norsk bokmål
	Polski
	Português (Brasil)
	Português (Portugal)
	Română
	Slovenčina
	Suomi
	Svenska
	Türkçe
	български
	Pусский
	Српски
	한국어
	中文 (普通话 (中国大陆))
	中文 (普通话 (香港))
	中文 (中文 (台灣))
	中文 (粵語 (香港))
	日本語
	Lingua latīna